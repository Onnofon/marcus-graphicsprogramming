﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour
{
    public GameObject flower;
    public GameObject collumn;
    public GameObject tree;
    public List<GameObject> flowers;

    public void CreateFlower(int x, float y, int z)
    {
        float number = Random.Range(0f, 3f);
        flowers.Add(this.gameObject);
        if (number < 2f && number > 0.2f)
        {
            Instantiate(flower, new Vector3(x, y, z), Quaternion.identity);
        }else if (number <= 0.15f && number >= 0.075f)
        {
            Instantiate(tree, new Vector3(x, y, z), Quaternion.identity);
        }
        else if (number <= 0.075f)
        {
            Instantiate(collumn, new Vector3(x, y-1f, z), Quaternion.identity);
        }
    }
}

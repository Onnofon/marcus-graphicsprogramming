﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moon : MonoBehaviour
{
    [Range(2, 256)]
    public int resolution = 10;

    [SerializeField, HideInInspector]
    MeshFilter[] meshFilters;
    MoonMesh[] terrainFaces;
    public Transform player;
    public float distance;

    private void OnValidate()
    {
        Initialize();
        GenerateMesh();
    }

    void Initialize()
    {
        if(meshFilters == null || meshFilters.Length == 0)
            meshFilters = new MeshFilter[6];

        terrainFaces = new MoonMesh[6];
        Vector3[] directions = { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
        for (int i = 0; i < 6; i++)
        {
            if (meshFilters[i] == null) {
                GameObject meshObj = new GameObject("mesh");
                meshObj.transform.parent = transform;

                meshObj.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                meshFilters[i] = meshObj.AddComponent<MeshFilter>();
                meshFilters[i].sharedMesh = new Mesh();
            }

            terrainFaces[i] = new MoonMesh(meshFilters[i].sharedMesh, resolution, directions[i]);
        }
    }

    public void Update()
    {
        /*distance = Vector3.Distance(this.transform.position, player.position);
        if(distance > 500)
        {
            resolution = 20;
        }
        else if(distance > 300){
            resolution = 50;
        }
        else
        {
            resolution = 150;
        }*/
    }
    void GenerateMesh()
    {
        foreach(MoonMesh face in terrainFaces)
        {
            face.GenerateMesh();
        }
    }
}
